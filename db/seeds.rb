# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

band_list = [
    [ "Zep", 6],
    [ "Park", 3],
    [ "Link", 6]
#already created 1 band
]

club_list = [

    [ "Hots", "Globe 4123"]
#already created 1 club
]
booking_list = [
    [ 1, 1, 40, "2014-05-15" ],
    [ 1, 2, 50, "2014-06-15" ],
    [ 2, 1, 40, "2014-07-15" ],
    [ 2, 2, 50, "2014-08-15" ],
    [ 3, 1, 40, "2014-09-15" ],
    [ 3, 2, 60, "2014-10-15" ],
    [ 4, 1, 40, "2014-11-15" ],
    [ 4, 2, 30, "2014-12-15" ]
]


band_list.each do |name, num_menbers|
  Band.create( name: name, num_menbers: num_menbers)

end
club_list.each do |n, address|
  Club.create( name: n, street_address: address)

end

booking_list.each do |b, c, f, d|
  Booking.create( band_id: b, club_id: c, fee: f, date: d)
end