json.array!(@bands) do |band|
  json.extract! band, :id, :name, :num_menbers
  json.url band_url(band, format: :json)
end
