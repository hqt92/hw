class Booking < ActiveRecord::Base
  belongs_to :band
  belongs_to :club


  validates :fee, presence: true,
            numericality: {greater_than:0}

end
