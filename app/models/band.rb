class Band < ActiveRecord::Base
  has_many :booking
  has_many :clubs, :through => :booking

  accepts_nested_attributes_for :clubs
end
