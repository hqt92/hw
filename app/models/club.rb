class Club < ActiveRecord::Base
  has_many :booking
  has_many :bands, through: :booking
end
